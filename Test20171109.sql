/* Home Test */

/* 1 */

CREATE TABLE dog(
  id SERIAL PRIMARY KEY,
  type TEXT,
  color TEXT
);

INSERT INTO dog(type, color) VALUES ('dog','black'), ('cat','gray'), ('dog','white'), ('fox','red'), ('dog','black');

CREATE TEMP TABLE dog_with_counter(
  id SERIAL PRIMARY KEY,
  type TEXT,
  color TEXT,
  counter INTEGER
);

INSERT INTO dog_with_counter (type, color, counter)
  SELECT type, color, COUNT(*)
  FROM dog
  GROUP BY (type, color);

SELECT * FROM dog_with_counter;

TRUNCATE dog RESTART IDENTITY;

ALTER TABLE dog
ADD COLUMN counter INTEGER;

INSERT INTO dog SELECT * FROM dog_with_counter;

/* 2 */

CREATE TABLE node_day_stat (
  cluster_id INTEGER,
  node_id INTEGER,
  manufacturer INTEGER
);

CREATE TABLE removed_node_log (
  cluster_id INTEGER,
  node_id INTEGER,
  manufacturer INTEGER,
  time TIMESTAMP
);

CREATE TABLE added_node_log (
  cluster_id INTEGER,
  node_id INTEGER,
  manufacturer INTEGER,
  time TIMESTAMP
);

/**
  1. Copy all data from node_day_stat
  table to the empty temp table named previous_day_node_stat
  before refresh.
*/

CREATE TEMP TABLE previous_day_node_stat(
  cluster_id INTEGER,
  node_id INTEGER,
  manufacturer INTEGER
);

INSERT INTO previous_day_node_stat SELECT * FROM node_day_stat;

/**
  2. Refresh table and then insert into it new data
*/

TRUNCATE node_day_stat RESTART IDENTITY;

INSERT INTO node_day_stat VALUES (1, 1, 1), (1, 2, 1);

/**
  3. Find removed rows: select from the table
  previous_day_node_stat rows that don't exist in
  current day data. Then insert this select result
  into removed_node_log table.
*/

INSERT INTO removed_node_log
SELECT *, NOW() FROM previous_day_node_stat
WHERE (cluster_id, node_id) NOT IN (
  SELECT cluster_id, node_id
  FROM node_day_stat
);

/**
  4. Find new added nodes: select from
  the table node_day_stat rows that don't exist
  in the added_node_log table. Then insert this select into
  added_node_log table.
*/

INSERT INTO added_node_log
SELECT *, NOW() FROM node_day_stat
WHERE (cluster_id, node_id) NOT IN (
  SELECT cluster_id, node_id
  FROM added_node_log
);


/**
  5. Now we can drop temp table.
*/

DROP TABLE previous_day_node_stat;

/**
  If we want to find 10 nodes removed since the first day,
  we can use this select:
*/

SELECT *
FROM removed_node_log
LIMIT 10;

/**
  If we want to find nodes that were added in past 10 days,
  we can use this:
*/

SELECT *
FROM added_node_log
WHERE (NOW() - time <= INTERVAL '10' day)