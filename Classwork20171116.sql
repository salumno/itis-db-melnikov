/*  Functions Practice  */

/* Fibonacci Numbers */
DO $$
DECLARE
  num_1 INTEGER := 1;
  num_2 INTEGER := 1;
  temp_sum INTEGER;
BEGIN
  FOR counter IN 1..10 LOOP
    RAISE NOTICE '% ', num_1;
    temp_sum := num_1 + num_2;
    num_1 := num_2;
    num_2 := temp_sum;
  END LOOP;
END;
$$;

/* Array Sort */
CREATE OR REPLACE FUNCTION bubbleSort(a INTEGER ARRAY) RETURNS INTEGER ARRAY AS $$
DECLARE
  length INTEGER := array_length(a, 1);
  tempElem INTEGER;
BEGIN
  FOR i IN 1..length LOOP
    FOR j IN 1..length - 1 LOOP
      IF a[j] > a[j + 1] THEN
        tempElem := a[j];
        a[j] := a[j + 1];
        a[j + 1] := tempElem;
      END IF;
    END LOOP;
  END LOOP;
  RETURN a;
END;
$$ LANGUAGE 'plpgsql';

SELECT bubbleSort('{5, 2, 9, 8, 1, 1, 6}');