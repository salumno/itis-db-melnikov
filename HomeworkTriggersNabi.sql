-- 1
CREATE TRIGGER hash_password_md5
BEFORE INSERT OR UPDATE ON "user" FOR EACH ROW EXECUTE PROCEDURE hash_md5_procedure();

CREATE OR REPLACE FUNCTION hash_md5_procedure() RETURNS TRIGGER as $$
BEGIN
  IF (TG_OP = 'INSERT') THEN
    NEW.password := md5(NEW.password);
    RETURN NEW;
  ELSIF (TG_OP = 'UPDATE') THEN
    IF (NEW.password != OLD.password) THEN
      NEW.password := md5(NEW.password);
    END IF;
    RETURN NEW;
  END IF;
END;
$$ LANGUAGE plpgsql;

INSERT INTO "user"(username, password, created_time) VALUES ('sema', 'qwerty', NOW());
UPDATE "user" SET password = '1234' WHERE id = 1;

-- 2
CREATE TABLE product (
  id SERIAL PRIMARY KEY,
  title VARCHAR(50),
  price FLOAT
);

CREATE TABLE product_history (
  product_id INTEGER,
  price_old FLOAT,
  title_old VARCHAR(50),
  price_new FLOAT,
  title_new VARCHAR(50),
  history_date DATE
);

CREATE TRIGGER product_log
BEFORE UPDATE OR DELETE ON product FOR EACH ROW EXECUTE PROCEDURE product_date_log();

CREATE OR REPLACE FUNCTION product_date_log() RETURNS TRIGGER as $$
BEGIN
  IF (TG_OP = 'UPDATE') THEN
    INSERT INTO product_history SELECT OLD.id, OLD.price, OLD.title, NEW.price, NEW.title, NOW();
    RETURN NEW;
  ELSEIF (TG_OP = 'DELETE') THEN
    INSERT INTO product_history(product_id, price_old, title_old, history_date) VALUES (OLD.id, OLD.price, OLD.title, NOW());
    RETURN NEW;
  END IF;
END;
$$ LANGUAGE plpgsql;

INSERT INTO product(title, price) VALUES('Jacket', 55.9), ('Pants', 9.00), ('Hat', 2.99);
UPDATE product SET title = 'Black Jacket' WHERE id = 1;
UPDATE product SET price = 55 WHERE id = 2;
DELETE FROM product WHERE id = 3;

-- 3
CREATE TABLE seo_page(
  id SERIAL PRIMARY KEY,
  link TEXT,
  old_link TEXT,
  title TEXT
);

INSERT INTO seo_page(link, title) VALUES ('vkontakte.ru', 'VKontakte Social Network'),
  ('krakatau.ru', 'Krakatau Antagonist Survival Kit');

CREATE TRIGGER seo_page_link_changed
BEFORE UPDATE ON seo_page FOR EACH ROW EXECUTE PROCEDURE log_seo_page_old_link();

CREATE OR REPLACE FUNCTION log_seo_page_old_link() RETURNS TRIGGER AS $$
BEGIN
  IF (OLD.link != NEW.link) THEN
    NEW.old_link := OLD.link;
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

UPDATE seo_page SET link = 'vk-ug.com' WHERE id = 1;
UPDATE seo_page SET link = 'krakatauwear.com' WHERE id = 2;

-- 4
CREATE TABLE "order" (
  id SERIAL PRIMARY KEY,
  created_time DATE,
  total INTEGER
);

CREATE TABLE order_product (
  id SERIAL PRIMARY KEY,
  order_id INTEGER REFERENCES "order"(id),
  product_id INTEGER REFERENCES product(id),
  price INTEGER,
  qty INTEGER,
  total INTEGER
);

CREATE TRIGGER qty_changed
BEFORE UPDATE ON order_product FOR EACH ROW EXECUTE PROCEDURE order_product_total_recount();

CREATE OR REPLACE FUNCTION order_product_total_recount() RETURNS TRIGGER as $$
DECLARE
  new_total FLOAT;
BEGIN
  IF (TG_OP = 'UPDATE') THEN
    IF (NEW.qty != OLD.qty) THEN
      new_total := OLD.price * NEW.qty;
      NEW.total := new_total;
      UPDATE "order" SET total = new_total WHERE id = OLD.order_id;
    END IF;
    IF (NEW.price != OLD.price) THEN
      new_total := NEW.price * OLD.qty;
      NEW.total := new_total;
      UPDATE "order" SET total = new_total WHERE id = OLD.order_id;
    END IF;
  ELSEIF (TG_OP = 'INSERT') THEN
    UPDATE "order" SET total = NEW.total WHERE id = NEW.order_id;
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;