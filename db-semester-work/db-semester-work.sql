/******* Tables' creating ********/
CREATE TABLE image(
  id SERIAL PRIMARY KEY,
  url VARCHAR(150) NOT NULL
);

CREATE TABLE "user" (
  id SERIAL PRIMARY KEY,
  name VARCHAR(20) NOT NULL
);

CREATE TABLE genre (
  id SERIAL PRIMARY KEY,
  name VARCHAR(20),
  description VARCHAR(140),
  image_id INTEGER REFERENCES image(id)
);

CREATE TABLE topic_category (
  id SERIAL PRIMARY KEY,
  name TEXT
);

CREATE TABLE topic (
  id SERIAL PRIMARY KEY,
  name VARCHAR(100) NOT NULL ,
  date DATE DEFAULT NOW(),
  author_id INTEGER REFERENCES "user"(id),
  image_id INTEGER REFERENCES image(id),
  category_id INTEGER REFERENCES topic_category(id),
  commentary_count INTEGER
);

CREATE TABLE news(
  id SERIAL PRIMARY KEY,
  title VARCHAR(140),
  description TEXT,
  date DATE DEFAULT NOW(),
  image_id INTEGER REFERENCES image(id),
  topic_id INTEGER REFERENCES topic(id)
);

CREATE TABLE album(
  id SERIAL PRIMARY KEY,
  name VARCHAR(50),
  author VARCHAR(50),
  image_id INTEGER REFERENCES image(id),
  track_count INTEGER
);

CREATE TABLE track (
  id SERIAL PRIMARY KEY,
  name VARCHAR(50) NOT NULL ,
  author VARCHAR(50),
  topic_id INTEGER REFERENCES topic(id),
  genre_id INTEGER REFERENCES genre(id),
  album_id INTEGER REFERENCES album(id),
  likes_count INTEGER DEFAULT 0,
  url VARCHAR(150) NOT NULL
);

CREATE TABLE commentary (
  id BIGSERIAL PRIMARY KEY,
  text TEXT NOT NULL ,
  author_id INTEGER REFERENCES "user"(id),
  date DATE DEFAULT NOW(),
  topic_id INTEGER REFERENCES topic(id)
);

CREATE TABLE user_category (
  id SERIAL PRIMARY KEY,
  category VARCHAR(20)
);

CREATE TABLE user_personal_data (
  user_id INTEGER REFERENCES "user"(id),
  login VARCHAR(30) NOT NULL,
  password TEXT NOT NULL,
  category_id INTEGER REFERENCES user_category(id)
);


CREATE TABLE user_liked_track (
  user_id INTEGER REFERENCES "user"(id),
  track_id INTEGER REFERENCES track(id)
);

CREATE TABLE user_liked_topic (
  user_id INTEGER REFERENCES "user"(id),
  topic_id INTEGER REFERENCES topic(id)
);

CREATE TABLE user_token(
  user_id INTEGER UNIQUE REFERENCES "user"(id),
  token TEXT UNIQUE
);

/**/

INSERT INTO topic_category(name) VALUES('news'), ('track'), ('common');

INSERT INTO user_category(category) VALUES ('admin'), ('user');

INSERT INTO "user"(name) VALUES ('Admin');

INSERT INTO user_personal_data VALUES (1, 'admin', 'admin', 1);

INSERT INTO image(url) VALUES ('/home/salumno/itis-beat-library/images/genre-gangsta-image.jpg'),
  ('/home/salumno/itis-beat-library/images/genre-hip-hop-image.jpg'),
  ('/home/salumno/itis-beat-library/images/genre-lyrical-image.jpg'),
  ('/home/salumno/itis-beat-library/images/genre-old-school-image.jpg'),
  ('/home/salumno/itis-beat-library/images/genre-rnb-image.jpg'),
  ('/home/salumno/itis-beat-library/images/genre-underground-image.jpg');

INSERT INTO genre(name, description, image_id) VALUES ('Gangsta', 'Real Gangsta Sh*t', 1),
  ('Hip-Hop', 'Only Real Hip-Hop', 2),
  ('Lyrical', 'Lyrics Masterpiece', 3),
  ('Old School', 'Real Sound From The Streets', 4),
  ('R&B', 'Blink Blink', 5),
  ('Underground', 'Not For Radio', 6);

/* Indexes */

CREATE INDEX idx_image_id ON image(id);

CREATE INDEX idx_track_id ON track(id);
CREATE INDEX idx_track_genre_id ON track(genre_id);
CREATE INDEX idx_track_topic_id ON track(topic_id);
CREATE INDEX idx_track_topic_likes ON track(likes_count DESC NULLS LAST);

CREATE INDEX idx_user_personal_data ON user_personal_data(user_id);
CREATE INDEX idx_commentary_topic_id ON commentary(topic_id);

CREATE INDEX idx_news_id ON news(id);
CREATE INDEX idx_news_topic_id ON news(topic_id);

CREATE INDEX idx_topic_id ON topic(id);
CREATE INDEX idx_topic_category_id ON topic(category_id);
CREATE INDEX idx_topic_comments_count ON topic(commentary_count DESC NULLS LAST);

/*********** Views *************/

-- Users' top by comments count
CREATE VIEW top_users_by_comments_count AS
  SELECT "user".id, "user".name, author_score.count
  FROM (
         SELECT author_id, COUNT(id) AS count
         FROM commentary
         GROUP BY (author_id)
       ) AS author_score JOIN "user"
      ON author_score.author_id = "user".id
  ORDER BY count DESC;

-- Users' top by topics' creating
CREATE VIEW top_users_by_created_topic AS
  SELECT "user".id, "user".name, count
  FROM (
         SELECT author_id, COUNT(id) AS count
         FROM topic
         WHERE category_id = 3
         GROUP BY author_id
       ) author_score JOIN "user"
      ON author_score.author_id = "user".id
  ORDER BY count DESC;

-- Genres' top by tracks' like count sum
CREATE VIEW top_genres_by_likes_sum AS
  SELECT id, name, like_sum
  FROM (
         SELECT genre_id, SUM(likes_count) like_sum
         FROM track
         GROUP BY genre_id
       ) genre_like_count JOIN genre
      ON genre_like_count.genre_id = genre.id
  ORDER BY like_sum DESC;

/*
 Logging tables and triggers
*/

/*************** Log tables **************/
CREATE TABLE log_track_add (
  track_id INTEGER,
  url TEXT,
  topic_id INTEGER,
  add_date TIMESTAMP
);

CREATE TABLE log_track_delete (
  track_id INTEGER,
  name VARCHAR(50),
  likes_count INTEGER,
  topic_id INTEGER,
  url TEXT,
  delete_date TIMESTAMP
);

CREATE TABLE log_news_delete (
  news_id INTEGER,
  title VARCHAR(140),
  description TEXT,
  topic_id INTEGER,
  image_id INTEGER,
  delete_date TIMESTAMP
);

CREATE TABLE log_topic_delete (
  topic_id INTEGER,
  name VARCHAR(100),
  author_id INTEGER,
  delete_date TIMESTAMP
);

CREATE TABLE log_news_change (
  news_id INTEGER,
  old_title VARCHAR(140),
  old_description TEXT,
  old_image_id INTEGER,
  new_title VARCHAR(140),
  new_description TEXT,
  new_image_id INTEGER,
  date TIMESTAMP
);

/*************** Triggers Functions*****************/

CREATE OR REPLACE FUNCTION track_delete_log() RETURNS TRIGGER AS $$
BEGIN
  INSERT INTO log_track_delete VALUES (OLD.id, OLD.name, OLD.likes_count, OLD.topic_id, OLD.url, NOW());
  RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION track_add_log() RETURNS TRIGGER AS $$
BEGIN
  INSERT INTO log_track_add VALUES (NEW.id, NEW.url, NEW.topic_id, NOW());
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION news_log() RETURNS TRIGGER AS $$
BEGIN
  IF (TG_OP = 'UPDATE') THEN
    INSERT INTO log_news_change
    VALUES (OLD.id, OLD.title, OLD.description, OLD.image_id, NEW.title, NEW.description, NEW.image_id, NOW());
    RETURN NEW;
  ELSEIF (TG_OP = 'DELETE') THEN
    INSERT INTO log_news_delete VALUES (OLD.id, OLD.title, OLD.description, OLD.topic_id, OLD.image_id, NOW());
    RETURN OLD;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION topic_delete_log() RETURNS TRIGGER AS $$
BEGIN
  INSERT INTO log_topic_delete VALUES (OLD.id, OLD.name, OLD.author_id, NOW());
  RETURN OLD;
END;
$$ LANGUAGE plpgsql;

/*************** Triggers Creating*****************/

/* Triggers for track table log*/
CREATE TRIGGER track_delete_log
BEFORE DELETE ON track FOR EACH ROW EXECUTE PROCEDURE track_delete_log();

CREATE TRIGGER track_add_log
AFTER INSERT ON track FOR EACH ROW EXECUTE PROCEDURE track_add_log();

/* Trigger for news table log*/
CREATE TRIGGER news_log
BEFORE UPDATE OR DELETE ON news FOR EACH ROW EXECUTE PROCEDURE news_log();

/* Trigger for topic table log*/
CREATE TRIGGER topic_delete_log
BEFORE DELETE ON topic FOR EACH ROW EXECUTE PROCEDURE topic_delete_log();

/*********** 'Denormalized' summary table and function for it **********/

/* Summary 'denormalized' table */
CREATE TABLE user_given_cookie (
  user_id INTEGER,
  name VARCHAR(20),
  login VARCHAR(30),
  password TEXT,
  category VARCHAR(20),
  token TEXT
);

-- Func that fill the table with accounts that have got a cookie right now
CREATE OR REPLACE FUNCTION user_cookie_func() RETURNS VOID AS $$
BEGIN
  TRUNCATE user_given_cookie;
  INSERT INTO user_given_cookie
    SELECT t2.user_id, name, login, password, category, user_token.token
    FROM (
      ("user" JOIN user_personal_data ON "user".id = user_personal_data.user_id) t1 JOIN user_category
      ON t1.category_id = user_category.id
    ) t2 JOIN user_token
    ON t2.user_id = user_token.user_id;
END;
$$ LANGUAGE plpgsql;

-- Example of invoking
SELECT user_cookie_func();

/******* Test *********/
DELETE FROM track WHERE id = 12;
DELETE FROM topic WHERE id = 29;
INSERT INTO track (name, author, topic_id, genre_id, likes_count, url) VALUES ('M.A.A.D. City','Kendrick Lamar', 30, 2, 0, '/home/hip-hop');
UPDATE news SET title = 'Jay Rock will drop new album very soon!' WHERE id = 7;
DELETE FROM news WHERE id = 7;