﻿CREATE TABLE department (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50)
);

INSERT INTO department(name) 
VALUES('IT'),('HR');

ALTER TABLE employee
ADD CONSTRAINT department_fk
FOREIGN KEY (department_id) REFERENCES department(id);

/*Отделы, где количество сотрудников не превышает 4*/
SELECT department.name
FROM department
JOIN (
    SELECT department_id, COUNT(employee.id)
    FROM employee
    GROUP BY department_id
    HAVING COUNT(employee.id) <= 4
) AS emp_count
ON department.id = emp_count.department_id;

/* Отделы с наиб суммарной ЗП */
SELECT name
FROM department
JOIN (
    SELECT department_id, SUM(salary)
    FROM employee
    GROUP BY department_id
) AS dep_salary_sum
ON department.id = dep_salary_sum.department_id
WHERE sum IN (
  SELECT MAX(sum_salary)
  FROM (
         SELECT department_id, SUM(salary) AS sum_salary
         FROM employee
         GROUP BY department_id
  ) as salary2
);

/* Имена сотрудников, у которых менеджер работает в другом отделе */
SELECT emp.name
FROM employee AS emp
JOIN employee AS man ON emp.manager_id = man.id
WHERE emp.department_id != man.department_id;