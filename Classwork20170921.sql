﻿CREATE TABLE employee (
    id SERIAL PRIMARY KEY,
    name TEXT,
    salary integer,
    manager_id integer,
    department_id integer
);

INSERT INTO employee(id, name, salary) 
VALUES(1, 'Vasya', 10);

INSERT INTO employee(id, name, salary) 
VALUES(2, 'Petya', 20), (3, 'Sergei', 30), (4, 'Nikola', 40), (5, 'Shimon', 50);

SELECT salary
FROM employee
WHERE name = 'Vasya';

UPDATE employee
SET salary = 20
WHERE name = 'Vasya';

DELETE FROM employee
WHERE name = 'Petya';

INSERT INTO employee(id, name, salary) 
VALUES(6, 'Kostya', 15), (7, 'Bulat', 20);

UPDATE employee
SET department_id = 1;

UPDATE employee
SET department_id = 2
WHERE id = 1 OR id = 3 OR id = 5;

/*1*/
SELECT department_id, MAX(salary)
FROM employee
GROUP BY department_id;

/*2*/
SELECT department_id, AVG(salary)
FROM employee
GROUP BY department_id
HAVING AVG(salary) > 10;

ALTER TABLE employee
ADD CONSTRAINT empl_manager_fk
FOREIGN KEY (manager_id) REFERENCES employee(id);

UPDATE employee
SET manager_id = 5;

UPDATE employee
SET manager_id = null
WHERE id = 5;

UPDATE employee
SET salary = 60
WHERE name = 'Vasya';

/*3*/
SELECT empl.name 
FROM employee AS empl
JOIN employee AS manager
ON empl.manager_id = manager.id
WHERE empl.salary > manager.salary;

/*4*/
SELECT name
FROM employee 
JOIN (
    SELECT department_id, MAX(salary) AS salary
    FROM employee
    GROUP BY department_id
) AS avg_salary
ON employee.department_id = avg_salary.department_id
WHERE employee.salary = avg_salary.salary;